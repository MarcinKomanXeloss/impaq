

public class ProductsDB {

    public static ProductWrapper getProductWrapper(long barCode){
        DatabaseMock dbMock = new DatabaseMock();
        Product product = dbMock.findProductByBarCode(barCode);
        return new ProductWrapper(product);
    }
}


