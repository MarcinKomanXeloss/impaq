

class ProductWrapper {

    private Product product;

    public ProductWrapper(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    public boolean productExists() {
        return product != null;
    }
}

