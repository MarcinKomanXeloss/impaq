import java.util.ArrayList;
import java.util.List;

public class PointOfSale {

    public static void main(String args[]) {
        List<ProductWrapper> productsToBuy = new ArrayList<>();
        double sum = 0;

        for (;;) {
            Long barCode;

            try {
                barCode = BarCodeScanner.scan();
            } catch (ScannerException e) {
                LCD.printError("Invalid bar-code");
                continue;
            }

            ProductWrapper productWrapper = ProductsDB.getProductWrapper(barCode);
            if (!productWrapper.productExists())
                LCD.printError("Product not found");
            else {
                productsToBuy.add(productWrapper);
                Product product = productWrapper.getProduct();
                LCD.printNameAndPrice(product.getProductName(), product.getProductPrice());
                sum = sum + product.getProductPrice();
            }

            goSleep();

            break; //implementation of exit if choose button on keyboard or smth. , for example enter (break here only for disable comp error)
        }
        LCD.printSumOnScreen(sum);
        new Printer().printReceipt(productsToBuy, sum);
    }

    private static void goSleep(){
        try {
            Thread.sleep(100);
        } catch (Exception ignored) {
            //ignore
        }
    }
}