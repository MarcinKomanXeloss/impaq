

public class Product {

    private String productName;
    private int productPrice;
    private long id;

    public String getProductName(){
        return productName;
    }

    public int getProductPrice(){
        return productPrice;
    }

    public long getId(){
        return id;
    }
}
